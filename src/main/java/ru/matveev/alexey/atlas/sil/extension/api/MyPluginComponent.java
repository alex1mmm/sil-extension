package ru.matveev.alexey.atlas.sil.extension.api;

public interface MyPluginComponent
{
    String getName();
}