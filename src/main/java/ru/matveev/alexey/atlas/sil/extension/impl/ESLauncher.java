package ru.matveev.alexey.atlas.sil.extension.impl;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.plugin.spring.scanner.annotation.component.ClasspathComponent;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.keplerrominfo.jira.commons.ivm.CustomFieldDescriptorRegistry;
import com.keplerrominfo.refapp.config.CommonPluginConfigurationService;
import com.keplerrominfo.refapp.config.HostConfigurationProvider;
import com.keplerrominfo.refapp.config.PluginInfoService;
import com.keplerrominfo.refapp.config.impl.PluginConfigurationServiceImpl;
import com.keplerrominfo.refapp.launcher.AbstractPluginLauncher;
import com.keplerrominfo.sil.lang.RoutineRegistry;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


import javax.inject.Inject;
import javax.inject.Named;

@ExportAsService(LifecycleAware.class)
@Named
public class ESLauncher extends AbstractPluginLauncher implements LifecycleAware {

    private static final Log LOG = LogFactory.getLog(ESLauncher.class);
    private final CustomFieldDescriptorRegistry customFieldDescriptorRegistry;
    private final ProjectManager projectManager;

    @Inject
    public ESLauncher(@ComponentImport EventPublisher eventPublisher, PluginInfoService pluginInfoService, @ComponentImport CommonPluginConfigurationService commonPluginConfigurationService, @ComponentImport HostConfigurationProvider hostConfigurationProvider, @ClasspathComponent PluginConfigurationServiceImpl pluginConfigurationService, @ComponentImport CustomFieldDescriptorRegistry customFieldDescriptorRegistry, @ComponentImport ProjectManager projectManager)
    {
        super(eventPublisher, pluginInfoService, hostConfigurationProvider, pluginConfigurationService);
        LOG.error("eslauncher constructor");
        this.customFieldDescriptorRegistry = customFieldDescriptorRegistry;
        this.projectManager = projectManager;
    }

    @Override
    public void doAtLaunch() {
        super.doAtLaunch();
        LOG.error("eslauncher doatlaunch");

        RoutineRegistry.register(new ReverseStringRoutine("myReverseString"));
    }

    @Override
    public void doAtStop() {
        LOG.error("eslauncher doatstop");

        try {
            RoutineRegistry.unregister("myReverseString");
        } catch(Exception e) {
            LOG.error("Failed to unregister!", e);
        }
        super.doAtStop();
    }
}
