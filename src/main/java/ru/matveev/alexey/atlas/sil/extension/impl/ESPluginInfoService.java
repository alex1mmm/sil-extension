package ru.matveev.alexey.atlas.sil.extension.impl;

import com.keplerrominfo.refapp.config.PluginInfoService;

import javax.annotation.Nonnull;
import javax.inject.Named;

@Named
public class ESPluginInfoService implements PluginInfoService {
    @Nonnull
    @Override
    public String getKey() {
        return "ru.matveev.alexey.atlas.sil.extension.sil-extension";
    }

    @Nonnull
    @Override
    public String getName() {
        return "Extended SIL";
    }
}
