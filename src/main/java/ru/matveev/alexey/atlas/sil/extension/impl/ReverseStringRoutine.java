package ru.matveev.alexey.atlas.sil.extension.impl;

import com.keplerrominfo.common.util.MutableString;
import com.keplerrominfo.sil.lang.*;
import com.keplerrominfo.sil.lang.type.TypeInst;

import java.util.List;

public class ReverseStringRoutine extends AbstractRoutine<MutableString> {
    private static final SILType[][] types = {{ TypeInst.STRING }};

    public ReverseStringRoutine(String name) {
        super(name, types);
    }

    @Override
    public SILType<MutableString> getReturnType() {
        return TypeInst.STRING;
    }

    @Override
    protected SILValue<MutableString> executeRoutine(SILContext context, List<SILValue<?>> silValues) {
        //AbstractRoutine checks the parameters and their types
        SILValue param = silValues.get(0);
        //We know for sure this is a string
        String val = param.toStringValue();
        //we calculate the reversed value
        String reversedVal = new StringBuilder(val).reverse().toString();
        //We'll prepare the return here
        return SILValueFactory.string(reversedVal);
    }

    @Override
    public String getParams() {
        return "(str)"; //that's all
    }
}
