package ut.ru.matveev.alexey.atlas.sil.extension;

import org.junit.Test;
import ru.matveev.alexey.atlas.sil.extension.api.MyPluginComponent;
import ru.matveev.alexey.atlas.sil.extension.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}